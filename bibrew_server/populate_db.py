import argparse
import csv

from sqlalchemy.exc import DBAPIError

from database import StockDatabase
from model import metadata, Grain, Hop, Yeast

parser = argparse.ArgumentParser(
    description="populate database, option to reinitialize it."
)
parser.add_argument("--reinit",
                    action="store_true",
                    help="Remove all data from all columns.")
args = parser.parse_args()

GRAIN_CSV = "bibrew_server/fixtures/grains.csv"
HOP_CSV = "bibrew_server/fixtures/hops.csv"
YEAST_CSV = "bibrew_server/fixtures/yeasts.csv"

database = StockDatabase()


def load_data(file_name):
    with open(file_name, newline='') as f:
        reader = csv.reader(f, delimiter=";")
        data = list(reader)
    data.pop(0)
    data = [[None if x == 'NULL' else x for x in row] for row in data]
    data = [[True if x == 'TRUE' else x for x in row] for row in data]
    data = [[False if x == 'FALSE' else x for x in row] for row in data]
    return data


if __name__ == "__main__":
    if args.reinit:
        metadata.drop_all(database.engine)
    try:
        data_g = load_data(GRAIN_CSV)
        data_h = load_data(HOP_CSV)
        data_y = load_data(YEAST_CSV)
    except OSError:
        print("File not Found")
        exit()
    try:
        for i in data_g:
            line_g = Grain(**{
                "name": i[0],
                "max_yield": i[1],
                "EBC": i[2],
                "max_proportion": i[3],
                "pH": i[4],
                "price": i[5]
            })
            database.session.add(line_g)

        for i in data_h:
            line_h = Hop(**{
                "name": i[0],
                "aromatic": i[1],
                "bitter": i[2],
                "alpha": i[3],
                "beta": i[4],
                "price": i[5]
            })
            database.session.add(line_h)

        for i in data_y:
            line_y = Yeast(**{
                "name": i[0],
                "producer": i[1],
                "type": i[2],
                "attenuation": i[3],
                "min_temp": i[4],
                "max_temp": i[5],
                "floculation": i[6],
                "price": i[7]
            })
            database.session.add(line_y)
        database.session.commit()
    except DBAPIError as exc:
        print(f"{exc}, rolling back...")
        database.session.rollback()
    finally:
        database.session.close()
