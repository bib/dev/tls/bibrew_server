from database import StockDatabase
from model import metadata

database = StockDatabase()
metadata.drop_all(database.engine)

