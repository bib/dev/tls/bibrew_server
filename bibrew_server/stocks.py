import json

from flask import request

from bibrew_server.app import app, auth
from bibrew_server.model import (
    Grain, GrainStock, Hop, HopStock, Yeast, YeastStock, Misc, MiscStock
)
from bibrew_server.schema import (
    GrainStockInfoSchema,
    HopStockInfoSchema,
    YeastStockInfoSchema,
    MiscStockInfoSchema,
)


@app.route("/grain_stocks")
@auth.login_required
def show_grain_stocks():
    grains = app.session.query(Grain).\
        join(GrainStock).\
        order_by(GrainStock.quantity.desc()).all()
    data = json.dumps(GrainStockInfoSchema(many=True).dump(grains))
    return data


@app.route("/hop_stocks")
@auth.login_required
def show_hop_stocks():
    hops = app.session.query(Hop).\
        join(HopStock).\
        order_by(HopStock.quantity.desc()).all()
    data = json.dumps(HopStockInfoSchema(many=True).dump(hops))
    return data


@app.route("/yeast_stocks")
@auth.login_required
def show_yeast_stocks():
    yeasts = app.session.query(Yeast).\
            join(YeastStock).\
            order_by(YeastStock.quantity.desc()).all()
    return json.dumps(YeastStockInfoSchema(many=True).dump(yeasts))

@app.route("/misc_stocks")
@auth.login_required
def show_misc_stocks():
    miscs = app.session.query(Misc).\
            join(MiscStock).\
            order_by(MiscStock.quantity.desc()).all()
    return json.dumps(MiscStockInfoSchema(many=True).dump(miscs))

@app.route("/grain_stock/<grain_id>", methods = ['GET', 'POST', "DELETE", "PUT"])
@auth.login_required
def grain_stock(grain_id):
    if request.method == 'GET':
        grain = app.session.query(Grain).\
            join(GrainStock).\
            filter(Grain.id==grain_id).one()
        return json.dumps(GrainStockInfoSchema().dump(grain))
    elif request.method == 'POST':
        data = request.form
        quantity = data["quantity"]
        new_grain = GrainStock(grain=grain_id, quantity=quantity)
        app.session.add(new_grain)
        app.session.commit()
        return "added"
    elif request.method == 'DELETE':
        query = app.session.query(GrainStock).\
            filter_by(grain=grain_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == "PUT":
        data = request.form
        quantity = data["quantity"]
        query = app.session.query(GrainStock).\
            filter_by(grain=grain_id).\
            one()
        query.quantity = quantity
        app.session.commit()
        return "updated"

@app.route("/hop_stock/<hop_id>", methods = ['GET', 'POST', "DELETE", "PUT"])
@auth.login_required
def hop_stock(hop_id):
    if request.method == 'GET':
        hop = app.session.query(Hop).\
            join(HopStock).\
            filter(id=hop_id).one()
        return json.dumps(HopStockInfoSchema().dump(hop))
    elif request.method == 'POST':
        data = request.form
        quantity = data["quantity"]
        new_hop = HopStock(hop=hop_id, quantity=quantity)
        app.session.add(new_hop)
        app.session.commit()
        return "added"
    elif request.method == 'DELETE':
        query = app.session.query(HopStock).\
            filter_by(hop=hop_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == "PUT":
        data = request.form
        quantity = data["quantity"]
        query = app.session.query(HopStock).\
            filter_by(hop=hop_id).\
            one()
        query.quantity = quantity
        app.session.commit()
        return "updated"
    
@app.route("/yeast_stock/<yeast_id>",
           methods = ['GET', 'POST', "DELETE", "PUT"])
@auth.login_required
def yeast_stock(yeast_id):
    if request.method == 'GET':
        yeast = app.session.query(Yeast).\
            join(YeastStock).\
            filter(id=yeast_id).one()
        return json.dumps(YeastStockInfoSchema().dump(yeast))
    elif request.method == 'POST':
        data = request.form
        quantity = data["quantity"]
        form = data["form"]
        new_yeast = YeastStock(yeast=yeast_id, quantity=quantity, form=form)
        app.session.add(new_yeast)
        app.session.commit()
        return "added"
    elif request.method == 'DELETE':
        query = app.session.query(YeastStock).\
            filter_by(yeast=yeast_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == "PUT":
        data = request.form
        quantity = data["quantity"]
        form = data["form"]
        query = app.session.query(YeastStock).\
            filter_by(yeast=yeast_id).\
            one()
        query.quantity = quantity
        query.form = form
        app.session.commit()
        return "updated"

@app.route("/misc_stock/<misc_id>", methods = ['GET', 'POST', "DELETE", "PUT"])
@auth.login_required
def misc_stock(misc_id):
    if request.method == 'GET':
        misc = app.session.query(Misc).\
            join(MiscStock).\
            filter(id=misc_id).one()
        return json.dumps(MiscStockInfoSchema().dump(misc))
    elif request.method == 'POST':
        data = request.form
        quantity = data["quantity"]
        new_misc = MiscStock(misc=misc_id, quantity=quantity)
        app.session.add(new_misc)
        app.session.commit()
        return "added"
    elif request.method == 'DELETE':
        query = app.session.query(MiscStock).\
            filter_by(misc=misc_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == "PUT":
        data = request.form
        quantity = data["quantity"]
        query = app.session.query(MiscStock).\
            filter_by(misc=misc_id).\
            one()
        query.quantity = quantity
        app.session.commit()
        return "updated"
