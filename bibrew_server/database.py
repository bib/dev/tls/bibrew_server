"""Database class and interaction"""
# Python base
import re

# Python autres
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session


class StockDatabase:
    def __init__(self):
        connect_str = re.sub("\n", "", self.get_connect())
        self.engine = create_engine(connect_str)
        self.session_local = sessionmaker(autocommit=False,
                                          autoflush=False,
                                          bind=self.engine)
        self.session = scoped_session(self.session_local)
        # self.import_data(g_bdd, h_bdd, l_bdd)

    def get_connect(self):
        connect_file = open('bibrew_server/database.ini', 'r')
        return connect_file.readline()

    def import_data(self, grains, hops, yeasts):
        with self.engine.connect() as con:
            data_g = pd.read_csv(grains, delimiter=";")
            data_g.columns = ["name",
                              "max_yield",
                              "EBC",
                              "max_proportion",
                              'pH',
                              "price"]
            data_h = pd.read_csv(hops, delimiter=";")
            data_h.columns = ['name',
                              'aromatic',
                              'bitter',
                              'alpha',
                              'beta',
                              'price']
            data_y = pd.read_csv(yeasts, delimiter=";")
            data_y.columns = ['name',
                              'producer',
                              'type',
                              'attenuation',
                              'min_temp',
                              'max_temp',
                              'floculation',
                              'price']
            data_g.to_sql(name="grains",
                          con=con,
                          if_exists="append",
                          index=False)
            data_h.to_sql(name="hops",
                          con=con,
                          if_exists="append",
                          index=False)
            data_y.to_sql(name="yeasts",
                          con=con,
                          if_exists="append",
                          index=False)
