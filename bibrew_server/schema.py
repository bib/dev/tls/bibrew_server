#!/usr/bin/env python3
"""Marshmallow schema"""
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields
from marshmallow_enum import EnumField

from bibrew_server.model import (
    Grain, GrainStock, Hop, HopStock, Yeast, YeastStock, Misc, MiscStock,
    YeastForm, YeastType, Floculation,
    RecipeWaterInput, RecipeGrainInput, RecipeHopInput, RecipeYeastInput,
    RecipeMiscInput,
    ArchiveWaterInput, ArchiveGrainInput, ArchiveHopInput, ArchiveYeastInput,
    ArchiveMiscInput,
    RecipeStep, Recipe, ArchiveRecipe,
    Conditioning, ConditioningEntry,
    Record, RecordDensity
)


class GrainSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Grain
        load_instance = True
        ordered = True


class GrainStockSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = GrainStock
        include_relationships = True
        load_instance = True
        ordered = True


class GrainStockInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Grain
        include_relationships = False
        load_instance = True
        ordered = True

    grain_stocks = fields.Nested(GrainStockSchema, many=True)


class HopSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Hop
        load_instance = True
        ordered = True


class HopStockSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = HopStock
        include_relationships = True
        load_instance = True
        ordered = True


class HopStockInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Hop
        include_relationships = False
        load_instance = True
        ordered = True

    hop_stocks = fields.Nested(HopStockSchema, many=True)


class YeastSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Yeast
        load_instance = True
        ordered = True

    floculation = EnumField(Floculation)
    type = EnumField(YeastType)


class YeastStockSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = YeastStock
        include_relationships = True
        load_instance = True
        ordered = True

    form = EnumField(YeastForm)


class YeastStockInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Yeast
        include_relationships = False
        load_instance = True
        ordered = True

    floculation = EnumField(Floculation)
    type = EnumField(YeastType)
    yeast_stocks = fields.Nested(YeastStockSchema, many=True)


class MiscSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Misc
        load_instance = True
        ordered = True


class MiscStockSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = MiscStock
        include_relationships = True
        load_instance = True
        ordered = True


class MiscStockInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Misc
        include_relationships = False
        load_instance = True
        ordered = True

    misc_stocks = fields.Nested(MiscStockSchema, many=True)


class RecipeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Recipe
        include_relationships = False
        load_instance = True


class RecipeWaterInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecipeWaterInput
        include_relationships = True
        load_instance = True

    step = EnumField(RecipeStep)


class RecipeGrainInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecipeGrainInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class RecipeGrainInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Grain
        include_relationships = True
        load_instance = True

    recipe_grain_input = fields.Nested(RecipeGrainInputSchema, many=True)
    grain_stocks = fields.Nested(GrainStockSchema, many=True)


class RecipeHopInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecipeHopInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class RecipeHopInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Hop
        include_relationships = True
        load_instance = True

    recipe_hop_input = fields.Nested(RecipeHopInputSchema, many=True)
    hop_stocks = fields.Nested(HopStockSchema, many=True)


class RecipeYeastInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecipeYeastInput
        include_relationships = True
        load_instance = True
        include_fk = True

    form = EnumField(YeastForm)


class RecipeYeastInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Yeast
        include_relationships = True
        load_instance = True

    floculation = EnumField(Floculation)
    type = EnumField(YeastType)
    recipe_yeast_input = fields.Nested(RecipeYeastInputSchema, many=True)
    yeast_stocks = fields.Nested(YeastStockSchema, many=True)


class RecipeMiscInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecipeMiscInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class RecipeMiscInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Misc
        include_relationships = True
        load_instance = True

    recipe_misc_input = fields.Nested(RecipeMiscInputSchema, many=True)
    misc_stocks = fields.Nested(MiscStockSchema, many=True)


class RecipeInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Recipe
        include_relationships = True
        load_instance = True

    water_input = fields.Nested(RecipeWaterInputSchema, many=True)
    grain_input = fields.Nested(RecipeGrainInfoSchema, many=True)
    hop_input = fields.Nested(RecipeHopInfoSchema, many=True)
    yeast_input = fields.Nested(RecipeYeastInfoSchema, many=True)
    misc_input = fields.Nested(RecipeMiscInfoSchema, many=True)


class RecordDensitySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RecordDensity
        include_relationships = True
        load_instance = True


class ConditioningSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Conditioning
        include_relationships = False
        load_instance = True


class ConditioningEntrySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ConditioningEntry
        include_relationships = True
        load_instance = True


class ConditioningEntriesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Conditioning
        include_relationships = True
        load_instance = True

    conditioning_entry = fields.Nested(ConditioningEntrySchema, many=True)


class RecordBasicSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Record
        include_fk = True
        load_instance = True


class RecordSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Record
        include_fk = True
        include_relationships = True
        load_instance = True

    record_density = fields.Nested(RecordDensitySchema, many=True)
    conditioning = fields.Nested(ConditioningEntriesSchema, many=True)


class ArchiveSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveRecipe
        include_relationships = False
        load_instance = True


class ArchiveWaterInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveWaterInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class ArchiveGrainInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveGrainInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class ArchiveGrainInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Grain
        include_relationships = True
        load_instance = True

    archive_grain_input = fields.Nested(ArchiveGrainInputSchema, many=True)


class ArchiveHopInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveHopInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class ArchiveHopInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Hop
        include_relationships = True
        load_instance = True

    archive_hop_input = fields.Nested(ArchiveHopInputSchema, many=True)


class ArchiveYeastInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveYeastInput
        include_relationships = True
        load_instance = True
        include_fk = True

    form = EnumField(YeastForm)


class ArchiveYeastInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Yeast
        include_relationships = True
        load_instance = True

    floculation = EnumField(Floculation)
    type = EnumField(YeastType)
    archive_yeast_input = fields.Nested(ArchiveYeastInputSchema, many=True)


class ArchiveMiscInputSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = ArchiveMiscInput
        include_relationships = True
        load_instance = True
        include_fk = True

    step = EnumField(RecipeStep)


class ArchiveMiscInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Misc
        include_relationships = True
        load_instance = True

    archive_misc_input = fields.Nested(ArchiveMiscInputSchema, many=True)
