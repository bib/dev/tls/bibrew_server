import json

from flask import request

from bibrew_server.app import app, auth
from bibrew_server.model import (
    Recipe, RecipeWaterInput,
    Grain, GrainStock, RecipeGrainInput,
    Hop, HopStock, RecipeHopInput,
    Yeast, YeastStock, RecipeYeastInput,
    Misc, MiscStock, RecipeMiscInput
)

from bibrew_server.schema import (
    RecipeSchema,
    RecipeGrainInfoSchema,
    RecipeHopInfoSchema,
    RecipeWaterInputSchema,
    RecipeYeastInfoSchema,
    RecipeMiscInfoSchema
)


@app.route("/recipes",
           methods=['GET', 'POST'])
@auth.login_required
def recipes():
    if request.method == "GET":
        recipes = app.session.query(Recipe).all()
        data = json.dumps(RecipeSchema(many=True).dump(recipes))
        return data
    elif request.method == "POST":
        data = json.loads(request.json)
        parent = data["parent"]
        name = data["name"]
        descri = data["descri"]
        volume = data["volume"]
        try:
            commit = data["commit"]
        except KeyError:
            commit = True
        new_recipe = Recipe(name = name,
                            parent = parent,
                            description = descri,
                            volume = volume)
        app.session.add(new_recipe)
        app.session.flush()
        new_id = new_recipe.id
        if commit:
            app.session.commit()
        return {"new_id": new_id}


@app.route("/recipe/<recipe_id>",
           methods=['GET', "DELETE", "PUT"])
@auth.login_required
def recipe(recipe_id):
    if request.method == 'GET':
        recipe = app.session.query(Recipe).\
            filter(Recipe.id == recipe_id).one()
        data = json.dumps(RecipeSchema().dump(recipe))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(Recipe).\
            filter_by(id=recipe_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return 'deleted'
    elif request.method == "PUT":
        data = json.loads(request.json)
        parent = data["parent"]
        name = data["name"]
        descri = data["descri"]
        volume = data["volume"]
        query = app.session.query(Recipe).\
            filter_by(id=recipe_id).\
            one()
        query.parent = parent
        query.name = name
        query.description = descri
        query.volume = volume
        app.session.commit()
        return 'edited recipe'


@app.route("/recipe/<recipe_id>/waters", methods=['GET', 'POST'])
@auth.login_required
def recipe_waters(recipe_id):
    if request.method == "GET":
        waters = app.session.query(RecipeWaterInput).\
            filter(RecipeWaterInput.recipe == recipe_id).\
            order_by(RecipeWaterInput.step)
        data = json.dumps(
            RecipeWaterInputSchema(many=True,
                                   exclude=["recipes"]).
            dump(waters)
        )
        return data
    elif request.method == "POST":
        data = request.form
        step = data["step"]
        volume = data["volume"]
        print(step, volume)
        to_add = RecipeWaterInput(recipe=recipe_id,
                                  step=step,
                                  volume=volume)
        app.session.add(to_add)
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "L'entrée existe déjà, pas de modification"
        return "added water"


@app.route("/recipe/<recipe_id>/grains", methods = ['GET', 'POST'])
@auth.login_required
def recipe_grains(recipe_id):
    if request.method == "GET":
        grains = app.session.query(Grain).\
            join(RecipeGrainInput).\
            join(GrainStock, isouter=True).\
            filter(RecipeGrainInput.recipe == recipe_id).\
            order_by(RecipeGrainInput.step,
                     RecipeGrainInput.duration.desc(),
                     RecipeGrainInput.quantity.desc())
        data = json.dumps(
            RecipeGrainInfoSchema(many=True,
                                  only=["name",
                                        "max_yield",
                                        "EBC",
                                        "recipe_grain_input.id",
                                        "recipe_grain_input.grains",
                                        "recipe_grain_input.step",
                                        "recipe_grain_input.duration",
                                        "recipe_grain_input.quantity",
                                        "recipe_grain_input.recipe",
                                        "grain_stocks.quantity",
                                        "grain_stocks.grains",
                                        "price"]).
            dump(grains)
        )
        return data
    elif request.method == "POST":
        data = request.form
        grain = data["grain"]
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        to_add = RecipeGrainInput(recipe=recipe_id,
                                  grain=grain,
                                  step=step,
                                  duration=duration,
                                  quantity=quantity)
        app.session.add(to_add)
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "L'entrée existe déjà, pas de modification"
        return "added grain"


@app.route("/recipe/<recipe_id>/hops", methods = ['GET', 'POST'])
@auth.login_required
def recipe_hops(recipe_id):
    if request.method == "GET":
        hops = app.session.query(Hop).\
            join(RecipeHopInput).\
            join(HopStock, isouter=True).\
            filter(RecipeHopInput.recipe == recipe_id).\
            order_by(RecipeHopInput.step,
                     RecipeHopInput.duration.desc(),
                     RecipeHopInput.quantity.desc())
        data = json.dumps(
            RecipeHopInfoSchema(many=True,
                                only=["name",
                                      "alpha",
                                      "recipe_hop_input.id",
                                      "recipe_hop_input.hops",
                                      "recipe_hop_input.step",
                                      "recipe_hop_input.duration",
                                      "recipe_hop_input.quantity",
                                      "recipe_hop_input.recipe",
                                      "hop_stocks.quantity",
                                      "hop_stocks.hops",
                                      "price"]).
            dump(hops)
        )
        return data
    elif request.method == 'POST':
        data = request.form
        hop = data["hop"]
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        to_add = RecipeHopInput(recipe=recipe_id,
                                hop=hop,
                                step=step,
                                duration=duration,
                                quantity=quantity)
        app.session.add(to_add)
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            print("erreur intégrité")
            return "L'entrée existe déjà, pas de modification"
        return "added hop"

@app.route("/recipe/<recipe_id>/yeasts", methods = ['GET', 'POST'])
@auth.login_required
def recipe_yeasts(recipe_id):
    if request.method == "GET":
        yeasts = app.session.query(Yeast).\
            join(RecipeYeastInput).\
            join(YeastStock, isouter=True).\
            filter(RecipeYeastInput.recipe == recipe_id).\
            order_by(RecipeYeastInput.delay)
        data = json.dumps(
            RecipeYeastInfoSchema(many=True,
                                  only=["name",
                                        "type",
                                        "attenuation",
                                        "recipe_yeast_input.id",
                                        "recipe_yeast_input.yeasts",
                                        "recipe_yeast_input.form",
                                        "recipe_yeast_input.delay",
                                        "recipe_yeast_input.recipe",
                                        "yeast_stocks.quantity",
                                        "yeast_stocks.yeasts",
                                        "price"]).
            dump(yeasts)
        )
        return data
    elif request.method == "POST":
        data = request.form
        yeast = data["yeast"]
        form = data["form"]
        delay = data["delay"]
        to_add = RecipeYeastInput(recipe=recipe_id,
                                  yeast=yeast,
                                  form=form,
                                  delay=delay)
        app.session.add(to_add)
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "L'entrée existe déjà, pas de modification"
        return "added yeast"

@app.route("/recipe/<recipe_id>/miscs", methods = ['GET', 'POST'])
@auth.login_required
def recipe_miscs(recipe_id):
    if request.method == "GET":
        miscs = app.session.query(Misc).\
            join(RecipeMiscInput).\
            join(MiscStock, isouter=True).\
            filter(RecipeMiscInput.recipe == recipe_id).\
            order_by(RecipeMiscInput.step,
                     RecipeMiscInput.duration.desc(),
                     RecipeMiscInput.quantity.desc())
        data = json.dumps(
            RecipeMiscInfoSchema(many=True,
                                 only=["name",
                                       "recipe_misc_input.id",
                                       "recipe_misc_input.miscs",
                                       "recipe_misc_input.step",
                                       "recipe_misc_input.duration",
                                       "recipe_misc_input.quantity",
                                       "recipe_misc_input.recipe",
                                       "misc_stocks.quantity",
                                       "price"]).
            dump(miscs)
        )
        return data
    elif request.method == "POST":
        data = request.form
        misc=data["misc"]
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        to_add = RecipeMiscInput(recipe=recipe_id,
                                 misc=misc,
                                 step=step,
                                 duration=duration,
                                 quantity=quantity)
        app.session.add(to_add)
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "L'entrée existe déjà, pas de modification"
        return "added misc"

@app.route("/recipe/<recipe_id>/water/<recipe_water_id>",
           methods = ['GET', "DELETE", "PUT"])
@auth.login_required
def recipe_water(recipe_id, recipe_water_id):
    if request.method == 'GET':
        water = app.session.query(RecipeWaterInput).\
            filter(RecipeWaterInput.id == recipe_water_id).\
            one()
        data =  json.dumps(RecipeWaterInputSchema().dump(water))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(RecipeWaterInput).\
            filter(RecipeWaterInput.id == recipe_water_id).one()
        app.session.delete(query)
        app.session.commit()
        return "deleted water"
    elif request.method == "PUT":
        data = request.form
        step = data["step"]
        volume = data["volume"]
        query = app.session.query(RecipeWaterInput).\
            filter(RecipeWaterInput.id == recipe_water_id).\
            one()
        query.step = step
        query.volume=volume
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "Update error"
        return "updated water"

@app.route("/recipe/<recipe_id>/grain/<recipe_grain_id>",
           methods = ['GET', "DELETE", "PUT"])
@auth.login_required
def recipe_grain(recipe_id, recipe_grain_id):
    if request.method == 'GET':
        grain = app.session.query(Grain).\
            join(RecipeGrainInput).\
            filter(RecipeGrainInput.id == recipe_grain_id).\
            one()
        data =  json.dumps(
            RecipeGrainInfoSchema(only=("name",
                                        "recipe_grain_input.step",
                                        "recipe_grain_input.duration",
                                        "recipe_grain_input.quantity")).
            dump(grain)
        )
        return data
    elif request.method == 'DELETE':
        query = app.session.query(RecipeGrainInput).\
            filter(RecipeGrainInput.id == recipe_grain_id).one()
        app.session.delete(query)
        app.session.commit()
        return "deleted grain"
    elif request.method == "PUT":
        data = request.form
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        query = app.session.query(RecipeGrainInput).\
            filter(RecipeGrainInput.id == recipe_grain_id).\
            one()
        query.step = step
        query.duration = duration
        query.quantity = quantity
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "Update error"
        return "updated grain"

@app.route("/recipe/<recipe_id>/hop/<recipe_hop_id>",
           methods = ['GET', "DELETE", "PUT"])
@auth.login_required
def recipe_hop(recipe_id, recipe_hop_id):
    if request.method == 'GET':
        hop = app.session.query(Hop).\
            join(RecipeHopInput).\
            filter(RecipeHopInput.id == recipe_hop_id).\
            one()
        data =  json.dumps(
            RecipeHopInfoSchema(only=("name",
                                      "recipe_hop_input.step",
                                      "recipe_hop_input.duration",
                                      "recipe_hop_input.quantity")).
            dump(hop)
        )
        return data
    elif request.method == 'DELETE':
        query = app.session.query(RecipeHopInput).\
            filter(RecipeHopInput.id == recipe_hop_id).one()
        app.session.delete(query)
        app.session.commit()
        return "deleted hop"        
    elif request.method == "PUT":
        data = request.form
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        query = app.session.query(RecipeHopInput).\
            filter(RecipeHopInput.id == recipe_hop_id).\
            one()
        query.step = step
        query.duration = duration
        query.quantity = quantity
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "Update error"
        return "updated hop"

@app.route("/recipe/<recipe_id>/yeast/<recipe_yeast_id>",
           methods = ['GET', "DELETE", "PUT"])
@auth.login_required
def recipe_yeast(recipe_id, recipe_yeast_id):
    if request.method == 'GET':
        yeast = app.session.query(Yeast).\
            join(RecipeYeastInput).\
            filter(RecipeYeastInput.id == recipe_yeast_id).\
            one()
        data =  json.dumps(
            RecipeYeastInfoSchema(only=("name",
                                        "recipe_yeast_input.step",
                                        "recipe_yeast_input.duration",
                                        "recipe_yeast_input.quantity")).
            dump(yeast)
        )
        return data
    elif request.method == 'DELETE':
        query = app.session.query(RecipeYeastInput).\
            filter(RecipeYeastInput.id == recipe_yeast_id).one()
        app.session.delete(query)
        app.session.commit()
        return "deleted yeast"
    elif request.method == "PUT":
        data = request.form
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        query = app.session.query(RecipeYeastInput).\
            filter(RecipeYeastInput.id == recipe_yeast_id).\
            one()
        query.form = form
        query.delay = delay
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "Update error"
        return "updated yeast"

@app.route("/recipe/<recipe_id>/misc/<recipe_misc_id>",
           methods = ['GET', "DELETE", "PUT"])
@auth.login_required
def recipe_misc(recipe_id, recipe_misc_id):
    if request.method == 'GET':
        misc = app.session.query(Misc).\
            join(RecipeMiscInput).\
            filter(RecipeMiscInput.id == recipe_misc_id).\
            one()
        data =  json.dumps(
            RecipeMiscInfoSchema(only=("name",
                                      "recipe_misc_input.step",
                                      "recipe_misc_input.duration",
                                      "recipe_misc_input.quantity")).
            dump(misc)
        )
        return data
    elif request.method == 'DELETE':
        query = sess.query(RecipeMiscInput).\
            filter(RecipeMiscInput.id == recipe_misc_id).one()
        sess.delete(query)
        sess.commit()
        return "deleted misc"
    elif request.method == "PUT":
        data = request.form
        step = data["step"]
        duration = data["duration"]
        quantity = data["quantity"]
        query = app.session.query(RecipeMiscInput).\
            filter(RecipeMiscInput.id == recipe_misc_id).\
            one()
        query.step = step
        query.duration = duration
        query.quantity = quantity
        try:
            app.session.commit()
        except exc.IntegrityError:
            app.session.rollback()
            return "Update error"
        return "updated misc"
