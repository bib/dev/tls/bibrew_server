#!/usr/bin/env python3
"""SQLAlchemy model"""
from datetime import datetime
from enum import Enum
from typing import Any

import sqlalchemy as sa  # type: ignore
from sqlalchemy.ext.declarative import declarative_base  # type: ignore
from sqlalchemy.orm import relationship, backref

Base: Any = declarative_base()
metadata = Base.metadata  # type: ignore


class RecipeStep(Enum):
    MASH = 10
    FILTER = 20
    RINSE = 30
    BOIL = 40
    WHIRLPOOL = 50
    DRYHOP = 60
    FERMENTATION = 70
    CONDITIONING = 80


steps_dict = {
    RecipeStep.MASH: "Empâtage",
    RecipeStep.FILTER: "Filtration",
    RecipeStep.RINSE: "Rinçage",
    RecipeStep.BOIL: "Ebullition",
    RecipeStep.WHIRLPOOL: "Whirlpool",
    RecipeStep.FERMENTATION: "Fermentation en cuve",
    RecipeStep.DRYHOP: "Dry-Hopping",
    RecipeStep.CONDITIONING: "Conditionnement"
}


class YeastForm(Enum):
    DRY = 1
    LIQUID = 2
    HARVEST = 3


class YeastType(Enum):
    ALE = 1
    LAGER = 2
    SAISON = 3
    VOSSKVEIK = 4


class Floculation(Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2


class Grain(Base):
    __tablename__ = 'grains'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    max_yield = sa.Column(sa.Float, nullable=True)
    EBC = sa.Column(sa.Float, nullable=True)
    max_proportion = sa.Column(sa.Float, nullable=True)
    pH = sa.Column(sa.Float, nullable=True)
    price = sa.Column(sa.Float, nullable=True)
    grain_stocks = relationship("GrainStock",
                                backref=backref("grains"),
                                cascade="all, delete-orphan")
    recipe_grain_input = relationship("RecipeGrainInput",
                                      backref=backref("grains"),
                                      cascade="all, delete-orphan")
    archive_grain_input = relationship("ArchiveGrainInput",
                                       backref=backref("grains"),
                                       cascade="all, delete-orphan")


class GrainStock(Base):
    __tablename__ = 'grain_stocks'

    grain = sa.Column(sa.ForeignKey("grains.id"), primary_key=True)
    quantity = sa.Column(sa.Float)


class Hop(Base):
    __tablename__ = 'hops'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    aromatic = sa.Column(sa.Boolean)
    bitter = sa.Column(sa.Boolean)
    alpha = sa.Column(sa.Float, nullable=True)
    beta = sa.Column(sa.Float, nullable=True)
    price = sa.Column(sa.Float, nullable=True)
    hop_stocks = relationship("HopStock",
                              backref="hops",
                              cascade="all, delete-orphan")
    recipe_hop_input = relationship("RecipeHopInput",
                                    backref=backref("hops"),
                                    cascade="all, delete-orphan")
    archive_hop_input = relationship("ArchiveHopInput",
                                     backref=backref("hops"),
                                     cascade="all, delete-orphan")


class HopStock(Base):
    __tablename__ = 'hop_stocks'

    hop = sa.Column(sa.ForeignKey("hops.id"), primary_key=True)
    quantity = sa.Column(sa.Float)


class Yeast(Base):
    __tablename__ = 'yeasts'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    producer = sa.Column(sa.String(64))
    type = sa.Column(sa.Enum(YeastType))
    attenuation = sa.Column(sa.Float, nullable=True)
    min_temp = sa.Column(sa.Float, nullable=True)
    max_temp = sa.Column(sa.Float, nullable=True)
    floculation = sa.Column(sa.Enum(Floculation), nullable=True)
    price = sa.Column(sa.Float, nullable=True)
    yeast_stocks = relationship("YeastStock",
                                backref=backref("yeasts"),
                                cascade="all, delete-orphan")
    recipe_yeast_input = relationship("RecipeYeastInput",
                                      backref=backref("yeasts"),
                                      cascade="all, delete-orphan")
    archive_yeast_input = relationship("ArchiveYeastInput",
                                       backref=backref("yeasts"),
                                       cascade="all, delete-orphan")


class YeastStock(Base):
    __tablename__ = 'yeast_stocks'
    yeast = sa.Column(sa.ForeignKey("yeasts.id"), primary_key=True)
    form = sa.Column(sa.Enum(YeastForm), nullable=False)
    quantity = sa.Column(sa.Float)


class Misc(Base):
    __tablename__ = 'miscs'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    price = sa.Column(sa.Float, nullable=True)
    description = sa.Column(sa.Text(), nullable=True)
    misc_stocks = relationship("MiscStock", backref="miscs")
    recipe_misc_input = relationship("RecipeMiscInput",
                                     backref=backref("miscs"),
                                     cascade="all, delete-orphan")
    archive_misc_input = relationship("ArchiveMiscInput",
                                      backref=backref("miscs"),
                                      cascade="all, delete-orphan")


class MiscStock(Base):
    __tablename__ = 'misc_stocks'

    misc = sa.Column(sa.ForeignKey("miscs.id"), primary_key=True)
    quantity = sa.Column(sa.Float)


class RecipeWaterInput(Base):
    __tablename__ = 'recipe_water_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("recipes.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    recipe_at_step = sa.UniqueConstraint(recipe, step)
    volume = sa.Column(sa.Float)


class RecipeGrainInput(Base):
    __tablename__ = 'recipe_grain_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("recipes.id"))
    grain = sa.Column(sa.ForeignKey("grains.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class RecipeHopInput(Base):
    __tablename__ = 'recipe_hop_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("recipes.id"))
    hop = sa.Column(sa.ForeignKey("hops.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class RecipeYeastInput(Base):
    __tablename__ = 'recipe_yeast_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("recipes.id"))
    yeast = sa.Column(sa.ForeignKey("yeasts.id"))
    delay = sa.Column(sa.Float)
    form = sa.Column(sa.Enum(YeastForm))
    description = sa.Column(sa.Text, nullable=True)


class RecipeMiscInput(Base):
    __tablename__ = 'recipe_misc_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("recipes.id"))
    misc = sa.Column(sa.ForeignKey("miscs.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float, nullable=True)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class Record(Base):
    __tablename__ = 'records'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    date = sa.Column(sa.DateTime, default=datetime.now)
    volume = sa.Column(sa.Float)
    initial_density = sa.Column(sa.Float, nullable=True)
    final_density = sa.Column(sa.Float, nullable=True)
    cold_crash = sa.Column(sa.Boolean, default=False)
    conditionned = sa.Column(sa.Boolean, default=False)
    carbonated = sa.Column(sa.Boolean, default=False)
    note = sa.Column(sa.Text, nullable=True)
    record_density = relationship("RecordDensity",
                                  backref="records",
                                  cascade="all, delete-orphan")
    conditioning = relationship("Conditioning",
                                backref="records",
                                cascade="all, delete-orphan")


class RecordDensity(Base):
    __tablename__ = 'record_densities'

    id = sa.Column(sa.Integer, primary_key=True)
    record = sa.Column(sa.ForeignKey("records.id"))
    date = sa.Column(sa.DateTime, default=datetime.now)
    label = sa.Column(sa.String(64))
    value = sa.Column(sa.Float)


class Conditioning(Base):
    __tablename__ = 'conditionings'

    id = sa.Column(sa.Integer, primary_key=True)
    record = sa.Column(sa.ForeignKey("records.id"))
    date = sa.Column(sa.DateTime, default=datetime.now)
    conditioning_entry = relationship("ConditioningEntry",
                                      backref="conditionings",
                                      cascade="all, delete-orphan")


class ConditioningEntry(Base):
    __tablename__ = 'conditioning_entries'

    id = sa.Column(sa.Integer, primary_key=True)
    conditioning = sa.Column(sa.ForeignKey("conditionings.id"))
    volume = sa.Column(sa.Float)
    quantity = sa.Column(sa.Integer)


class Recipe(Base):
    __tablename__ = 'recipes'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    parent = sa.Column(sa.Integer, nullable=True)
    name_at_parent = sa.UniqueConstraint(name, parent)
    description = sa.Column(sa.Text)
    volume = sa.Column(sa.Float, sa.CheckConstraint("volume>0"))
    water_input = relationship("RecipeWaterInput",
                               backref="recipes",
                               cascade="all, delete-orphan")
    grain_input = relationship("RecipeGrainInput",
                               backref="recipes",
                               cascade="all, delete-orphan")
    hop_input = relationship("RecipeHopInput",
                             backref="recipes",
                             cascade="all, delete-orphan")
    yeast_input = relationship("RecipeYeastInput",
                               backref="recipes",
                               cascade="all, delete-orphan")
    misc_input = relationship("RecipeMiscInput",
                              backref="recipes",
                              cascade="all, delete-orphan")


class ArchiveWaterInput(Base):
    __tablename__ = 'archive_water_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    volume = sa.Column(sa.Float)


class ArchiveGrainInput(Base):
    __tablename__ = 'archive_grain_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    grain = sa.Column(sa.ForeignKey("grains.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class ArchiveHopInput(Base):
    __tablename__ = 'archive_hop_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    hop = sa.Column(sa.ForeignKey("hops.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class ArchiveYeastInput(Base):
    __tablename__ = 'archive_yeast_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    yeast = sa.Column(sa.ForeignKey("yeasts.id"))
    delay = sa.Column(sa.Float)
    form = sa.Column(sa.Enum(YeastForm))
    description = sa.Column(sa.Text, nullable=True)


class ArchiveMiscInput(Base):
    __tablename__ = 'archive_misc_inputs'

    id = sa.Column(sa.Integer, primary_key=True)
    recipe = sa.Column(sa.ForeignKey("archive_recipes.id"))
    misc = sa.Column(sa.ForeignKey("miscs.id"))
    step = sa.Column(sa.Enum(RecipeStep))
    duration = sa.Column(sa.Float, nullable=True)
    quantity = sa.Column(sa.Float)
    description = sa.Column(sa.Text, nullable=True)


class ArchiveRecipe(Base):
    __tablename__ = 'archive_recipes'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    parent = sa.Column(sa.Integer, nullable=True)
    description = sa.Column(sa.Text)
    volume = sa.Column(sa.Float, sa.CheckConstraint("volume>0"))
    water_input = relationship("ArchiveWaterInput",
                               backref="archive_recipes",
                               cascade="all, delete-orphan")
    grain_input = relationship("ArchiveGrainInput",
                               backref="archive_recipes",
                               cascade="all, delete-orphan")
    hop_input = relationship("ArchiveHopInput",
                             backref="archive_recipes",
                             cascade="all, delete-orphan")
    yeast_input = relationship("ArchiveYeastInput",
                               backref="archive_recipes",
                               cascade="all, delete-orphan")
    misc_input = relationship("ArchiveMiscInput",
                              backref="archive_recipes",
                              cascade="all, delete-orphan")
    record = relationship("Record",
                          backref="archive_recipes",
                          cascade="all, delete-orphan")
