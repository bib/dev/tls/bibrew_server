"""Flask app with routing and methods"""
# TODO : Ajouter messages pour debug.

from flask import Flask
from flask_httpauth import HTTPBasicAuth
# from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.orm import scoped_session
from logbook import Logger, FileHandler
from bibrew_server.database import StockDatabase

FileHandler("bibrew_server.log")

log = Logger("Bibrew server logger")

database = StockDatabase()

app = Flask(__name__)
app.session = scoped_session(database.session_local)

auth = HTTPBasicAuth()

with open("bibrew_server/bibrew_server.ini") as f:
    password = f.read()

users = {
    "bras-soeur": password
}


@app.teardown_appcontext
def shutdown_session(exception=None, *args, **kwargs):
    app.session.remove()


@auth.verify_password
def authenticate(username, password):
    if username in users and \
            users.get(username) == password:
        return username


@app.route('/')
@auth.login_required
def index():
    return "Hello, {}!".format(auth.current_user())


@app.route("/commit", methods=['POST'])
@auth.login_required
def commit():
    app.session.commit()


import bibrew_server.stocks
import bibrew_server.rawmat_infos
import bibrew_server.recipes
import bibrew_server.records
