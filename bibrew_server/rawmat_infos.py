import json
from flask import request

from bibrew_server.app import app, auth
from bibrew_server.model import (
    Grain, Hop, Yeast, Misc
)
from bibrew_server.schema import (
    GrainSchema, HopSchema, YeastSchema, MiscSchema
)

@app.route("/grains_count", methods = ['GET'])
@auth.login_required
def grains_count():
    nb = app.session.query(Grain).count()
    return {"nb": nb}

@app.route("/hops_count", methods = ['GET'])
@auth.login_required
def hops_count():
    nb = app.session.query(Hop).count()
    return {"nb": nb}

@app.route("/yeasts_count", methods = ['GET'])
@auth.login_required
def yeasts_count():
    nb = app.session.query(Yeast).count()
    return {"nb": nb}

@app.route("/miscs_count", methods = ['GET'])
@auth.login_required
def miscs_count():
    nb = app.session.query(Misc).count()
    return {"nb": nb}

@app.route("/grains", methods = ['GET', 'POST'])
@auth.login_required
def grains():
    if request.method == 'GET':
        grains = app.session.query(Grain)
        return json.dumps(GrainSchema(many=True).dump(grains))
    elif request.method == 'POST':
        data = request.get_json(force=True)
        grain = Grain(name = data.get("name"),
                      max_yield = data.get("max_yield"),
                      EBC = data.get("EBC"),
                      max_proportion = data.get("max_proportion"),
                      pH = data.get("pH"),
                      price = data.get("price"))
        app.session.add(grain)
        app.session.flush()
        new_id = grain.id
        app.session.commit()
        return {"new_id": new_id}
                      
@app.route("/hops", methods = ['GET', 'POST'])
@auth.login_required
def hops():
    if request.method == 'GET':
        hops = app.session.query(Hop)
        return json.dumps(HopSchema(many=True).dump(hops))
    elif request.method == 'POST':
        data = request.get_json(force=True)
        hop = Hop(name = data["name"],
                  aromatic = data["aromatic"],
                  bitter = data["bitter"],
                  alpha = data["alpha"],
                  beta = data["beta"],
                  price = data["price"])
        app.session.add(hop)
        app.session.flush()
        new_id = hop.id
        app.session.commit()
        return {"new_id": new_id}

@app.route("/yeasts", methods = ['GET', 'POST'])
@auth.login_required
def yeasts():
    if request.method == 'GET':
        yeasts = app.session.query(Yeast).\
            order_by(Yeast.producer, Yeast.name).\
            all()
        return json.dumps(YeastSchema(many=True,
                                      only=["id",
                                            "name",
                                            "producer",
                                            "type",
                                            "attenuation",
                                            "min_temp",
                                            "max_temp",
                                            "floculation",
                                            "price"]).dump(yeasts))
    elif request.method == 'POST':
        data = request.get_json(force=True)
        yeast = Yeast(name = data["name"],
                      producer = data["producer"],
                      type = data["type"],
                      attenuation = data["attenuation"],
                      min_temp = data["min_temp"],
                      max_temp = data["max_temp"],
                      floculation = data["floculation"],
                      price = data["price"])
        app.session.add(yeast)
        app.session.flush()
        new_id = yeast.id
        app.session.commit()
        return {"new_id": new_id}

@app.route("/miscs", methods = ['GET', 'POST'])
@auth.login_required
def miscs():
    if request.method == 'GET':
        miscs = app.session.query(Misc)
        miscs_json = json.dumps(MiscSchema(many=True).dump(miscs))
        return miscs_json
    elif request.method == 'POST':
        data = request.get_json(force=True)
        misc = Misc(name = data["name"],
                    description = data["description"],
                    price = data["price"])
        app.session.add(misc)
        app.session.flush()
        new_id = misc.id
        app.session.commit()
        return {"new_id": new_id}

@app.route("/grains_types", methods = ['GET'])
@auth.login_required
def grains_types():
    grains_cols = Grain.__table__.c
    grains_types = {}
    for column in grains_cols:
        grains_types[column.name] = column.type.python_type.__name__
    return json.dumps(grains_types)

@app.route("/hops_types", methods = ['GET'])
@auth.login_required
def hops_types():
    hops_cols = Hop.__table__.c
    hops_types = {}
    for column in hops_cols:
        hops_types[column.name] = column.type.python_type.__name__
    return json.dumps(hops_types)

@app.route("/yeasts_types", methods = ['GET'])
@auth.login_required
def yeasts_types():
    yeasts_cols = Yeast.__table__.c
    yeasts_types = {}
    for column in yeasts_cols:
        yeasts_types[column.name] = column.type.python_type.__name__
    return json.dumps(yeasts_types)

@app.route("/miscs_types", methods = ['GET'])
@auth.login_required
def miscs_types():
    miscs_cols = Misc.__table__.c
    miscs_types = {}
    for column in miscs_cols:
        miscs_types[column.name] = column.type.python_type.__name__
    print(miscs_types)
    return json.dumps(miscs_types)
    
@app.route("/grain/<grain_id>", methods = ['DELETE', 'PUT'])
@auth.login_required
def grain(grain_id):
    if request.method == 'DELETE':
        query = app.session.query(Grain).\
            filter_by(id=grain_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == 'PUT':
        data = request.form
        query = app.session.query(Grain).\
            filter_by(id=grain_id).\
            one()
        query.name = data["name"]
        query.max_yield = data["max_yield"]
        query.EBC = data["EBC"]
        query.max_proportion = data["max_prop"]
        query.pH = data["pH"]
        query.price = data["price"]
        app.session.commit()
        return "updated"
        

@app.route("/hop/<hop_id>", methods = ['DELETE', 'PUT'])
@auth.login_required
def hop(hop_id):
    if request.method == 'DELETE':
        query = app.session.query(Hop).\
            filter_by(id=hop_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == 'PUT':
        data = request.form
        query = app.session.query(Hop).\
            filter_by(id=hop_id).\
            one()
        query.name = data["name"]
        query.aromatic = data["aromatic"]
        query.bitter = data["bitter"]
        query.alpha = data["alpha"]
        query.beta = data["beta"]
        query.price = data["price"]
        app.session.commit()
        return "updated"

@app.route("/yeast/<yeast_id>", methods = ['DELETE', 'PUT'])
@auth.login_required
def yeast(yeast_id):
    if request.method == 'DELETE':
        query = app.session.query(Yeast).\
            filter_by(id=yeast_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == 'PUT':
        data = request.form
        query = app.session.query(Yeast).\
            filter_by(id=yeast_id).\
            one()
        query.name = data["name"]
        query.producer = data["producer"]
        query.type = data["type"]
        query.attenuation = data["attenuation"]
        query.min_temp = data["min_temp"]
        query.max_temp = data["max_temp"]
        query.floculation = data["floculation"]
        query.price = data["price"]
        app.session.commit()
        return "updated"

@app.route("/misc/<misc_id>", methods = ['DELETE', 'PUT'])
@auth.login_required
def misc(misc_id):
    if request.method == 'DELETE':
        query = app.session.query(Misc).\
            filter_by(id=misc_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return "deleted"
    elif request.method == 'PUT':
        data = request.form
        query = app.session.query(Misc).\
            filter_by(id=misc_id).\
            one()
        query.name = data["name"]
        query.description = data["description"]
        query.price = data["price"]
        app.session.commit()
        return "updated"
