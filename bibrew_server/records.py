import json

from flask import request

from bibrew_server.app import app, auth, database
from bibrew_server.model import (
    Grain, Hop, Yeast, Misc,
    Recipe, RecipeWaterInput,
    RecipeGrainInput, RecipeHopInput, RecipeYeastInput, RecipeMiscInput,
    ArchiveRecipe, ArchiveWaterInput,
    ArchiveGrainInput, ArchiveHopInput, ArchiveYeastInput, ArchiveMiscInput,
    Record, RecordDensity,
    Conditioning, ConditioningEntry
)

from bibrew_server.schema import (
    RecipeSchema,
    ArchiveWaterInputSchema, ArchiveGrainInfoSchema,
    ArchiveHopInfoSchema, ArchiveYeastInfoSchema, ArchiveMiscInfoSchema,
    RecordBasicSchema, RecordSchema,
    ConditioningSchema, ConditioningEntrySchema, ConditioningEntriesSchema
)


@app.route("/records", methods=['GET', 'POST'])
@auth.login_required
def records():
    if request.method == "GET":
        records = app.session.query(Record).\
            order_by(Record.date.desc()).\
            all()
        data = json.dumps(RecordBasicSchema(
            many=True,
            only=["id",
                  "recipe",
                  "date",
                  "volume",
                  "initial_density",
                  "final_density",
                  "cold_crash",
                  "conditionned",
                  "carbonated"]
        ).dump(records))
        return data
    elif request.method == "POST":
        data = json.loads(request.json)
        recipe_id = data["recipe_id"]

        # Archiving recipe
        recipe = app.session.query(Recipe).\
            filter(Recipe.id == recipe_id).one()
        archive_recipe = ArchiveRecipe(name=recipe.name,
                                       parent=recipe.parent,
                                       description=recipe.description,
                                       volume=recipe.volume)
        app.session.add(archive_recipe)
        app.session.flush()
        archive_id = archive_recipe.id

        water_input = app.session.query(RecipeWaterInput).\
            filter(RecipeWaterInput.recipe == recipe_id).all()
        for row in water_input:
            archive_water = ArchiveWaterInput(
                recipe=archive_id,
                step=row.step,
                volume=row.volume
            )
            app.session.add(archive_water)

        grain_input = app.session.query(RecipeGrainInput).\
            filter(RecipeGrainInput.recipe == recipe_id).all()
        for row in grain_input:
            archive_grain = ArchiveGrainInput(
                recipe=archive_id,
                grain=row.grain,
                step=row.step,
                duration=row.duration,
                quantity=row.quantity,
                description=row.description
            )
            app.session.add(archive_grain)

        hop_input = app.session.query(RecipeHopInput).\
            filter(RecipeHopInput.recipe == recipe_id).all()
        for row in hop_input:
            archive_hop = ArchiveHopInput(
                recipe=archive_id,
                hop=row.hop,
                step=row.step,
                duration=row.duration,
                quantity=row.quantity,
                description=row.description
            )
            app.session.add(archive_hop)

        yeast_input = app.session.query(RecipeYeastInput).\
            filter(RecipeYeastInput.recipe == recipe_id).all()
        for row in yeast_input:
            archive_yeast = ArchiveYeastInput(
                recipe=archive_id,
                yeast=row.yeast,
                delay=row.delay,
                form=row.form,
                description=row.description
            )
            app.session.add(archive_yeast)

        misc_input = app.session.query(RecipeMiscInput).\
            filter(RecipeMiscInput.recipe == recipe_id).all()
        for row in misc_input:
            archive_misc = ArchiveMiscInput(
                recipe=archive_id,
                misc=row.misc,
                step=row.step,
                duration=row.duration,
                quantity=row.quantity,
                description=row.description
            )
            app.session.add(archive_misc)

        # Creating record
        date = data["date"]
        volume = data["volume"]
        initial_density = data["initial_density"]
        new_record = Record(recipe=archive_id,
                            date=date,
                            volume=volume,
                            initial_density=initial_density)
        app.session.add(new_record)
        app.session.flush()
        new_id = new_record.id

        try:
            commit = data["commit"]
        except KeyError:
            commit = True
        if commit:
            app.session.commit()
        return {"new_id": new_id}


@app.route("/record/<record_id>/initial_density", methods=['PUT'])
@auth.login_required
def init_density(record_id):
    data = json.loads(request.json)
    density = data["initial_density"]
    query = app.session.query(Record).\
        filter_by(id=record_id).\
        one()
    query.initial_density = density
    app.session.commit()
    return "added initial density"


@app.route("/record/<record_id>/final_density", methods=['PUT'])
@auth.login_required
def final_density(record_id):
    data = json.loads(request.json)
    density = data["final_density"]
    query = app.session.query(Record).\
        filter_by(id=record_id).\
        one()
    query.final_density = density
    app.session.commit()
    return "added final density"


@app.route("/record/<record_id>",
           methods=['GET', "DELETE", "PUT"])
@auth.login_required
def record(record_id):
    if request.method == 'GET':
        record = app.session.query(Record).\
            filter(Record.id == record_id).one()
        data = json.dumps(RecordSchema().dump(record))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(Record).\
            filter_by(id=record_id).\
            one()
        recipe = app.session.query(ArchiveRecipe).\
            filter(ArchiveRecipe.id == query.recipe)
        app.session.delete(recipe)
        app.session.delete(query)
        app.session.commit()
        return 'deleted'
    elif request.method == "PUT":
        data = json.loads(request.json)
        recipe_id = data["recipe_id"]
        date = data["date"]
        volume = data["volume"]
        initial_density = data["initial_density"]
        final_density = data["final_density"]
        query = app.session.query(Record).\
            filter_by(id=record_id).\
            one()
        query.recipe_id = recipe_id
        query.date = date
        query.volume = volume
        query.initial_density = initial_density
        query.final_density = final_density
        app.session.commit()
        return 'edited record'


@app.route("/record/recipes",
           methods=['GET'])
@auth.login_required
def recorded_recipes():
    recipes = app.session.query(ArchiveRecipe).all()
    data = json.dumps(RecipeSchema(many=True).dump(recipes))
    return data


@app.route("/record/<record_id>/recipe/infos",
           methods=['GET'])
@auth.login_required
def record_view_recipe(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = record.recipe
    recipe = app.session.query(ArchiveRecipe).\
        filter(ArchiveRecipe.id == recipe_id).one()
    data = json.dumps(RecipeSchema().dump(recipe))
    return data


@app.route("/record/<record_id>/recipe/waters",
           methods=['GET'])
@auth.login_required
def record_recipe_water(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = record.recipe
    water = app.session.query(ArchiveWaterInput).\
        filter(ArchiveWaterInput.recipe == recipe_id).\
        order_by(ArchiveWaterInput.step).all()
    data = json.dumps(ArchiveWaterInputSchema(many=True).
                      dump(water))
    return data


@app.route("/record/<record_id>/recipe/grains",
           methods=['GET'])
@auth.login_required
def record_recipe_grain(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = int(record.recipe)
    grains = app.session.query(Grain).\
        join(ArchiveGrainInput).\
        filter(ArchiveGrainInput.recipe == recipe_id).\
        order_by(ArchiveGrainInput.step,
                 ArchiveGrainInput.duration.desc(),
                 ArchiveGrainInput.quantity.desc())
    data = json.dumps(
        ArchiveGrainInfoSchema(many=True,
                               only=["name",
                                     "max_yield",
                                     "EBC",
                                     "archive_grain_input.id",
                                     "archive_grain_input.grains",
                                     "archive_grain_input.step",
                                     "archive_grain_input.duration",
                                     "archive_grain_input.quantity",
                                     "archive_grain_input.recipe",
                                     "price"]).
        dump(grains)
    )
    return data


@app.route("/record/<record_id>/recipe/hops",
           methods=['GET'])
@auth.login_required
def record_recipe_hop(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = record.recipe
    hops = app.session.query(Hop).\
        join(ArchiveHopInput).\
        filter(ArchiveHopInput.recipe == recipe_id).\
        order_by(ArchiveHopInput.step,
                 ArchiveHopInput.duration.desc(),
                 ArchiveHopInput.quantity.desc())
    data = json.dumps(
        ArchiveHopInfoSchema(many=True,
                             only=["name",
                                   "alpha",
                                   "archive_hop_input.id",
                                   "archive_hop_input.hops",
                                   "archive_hop_input.step",
                                   "archive_hop_input.duration",
                                   "archive_hop_input.quantity",
                                   "archive_hop_input.recipe",
                                   "price"]).
        dump(hops)
    )
    return data


@app.route("/record/<record_id>/recipe/yeasts",
           methods=['GET'])
@auth.login_required
def record_recipe_yeast(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = record.recipe
    yeasts = app.session.query(Yeast).\
        join(ArchiveYeastInput).\
        filter(ArchiveYeastInput.recipe == recipe_id).\
        order_by(ArchiveYeastInput.delay)
    data = json.dumps(
        ArchiveYeastInfoSchema(many=True,
                               only=["name",
                                     "type",
                                     "attenuation",
                                     "archive_yeast_input.id",
                                     "archive_yeast_input.yeasts",
                                     "archive_yeast_input.form",
                                     "archive_yeast_input.delay",
                                     "archive_yeast_input.recipe",
                                     "price"]).
        dump(yeasts)
    )
    return data


@app.route("/record/<record_id>/recipe/miscs",
           methods=['GET'])
@auth.login_required
def record_recipe_misc(record_id):
    record = app.session.query(Record).\
        filter(Record.id == record_id).one()
    recipe_id = record.recipe
    miscs = app.session.query(Misc).\
        join(ArchiveMiscInput).\
        filter(ArchiveMiscInput.recipe == recipe_id).\
        order_by(ArchiveMiscInput.step,
                 ArchiveMiscInput.duration.desc(),
                 ArchiveMiscInput.quantity.desc())
    data = json.dumps(
        ArchiveMiscInfoSchema(many=True,
                              only=["name",
                                    "archive_misc_input.id",
                                    "archive_misc_input.miscs",
                                    "archive_misc_input.step",
                                    "archive_misc_input.duration",
                                    "archive_misc_input.quantity",
                                    "archive_misc_input.recipe",
                                    "price"]).
        dump(miscs)
    )
    return data


@app.route("/record/<record_id>/densities",
           methods=['GET', 'POST'])
@auth.login_required
def densities(record_id):
    if request.method == 'GET':
        record = app.session.query(Record).\
            join(RecordDensity).\
            filter(Record.id == record_id).one()
        data = json.dumps(RecordSchema().dump(record))
        return data
    elif request.method == 'POST':
        data = json.loads(request.json)
        date = data["date"]
        label = data["label"]
        value = data["value"]
        try:
            commit = data["commit"]
        except KeyError:
            commit = True
        new_density = RecordDensity(record=record_id,
                                    date=date,
                                    label=label,
                                    value=value)
        app.session.add(new_density)
        app.session.flush()
        new_id = new_density.id
        if commit:
            app.session.commit()
        return {"new_id": new_id}


@app.route("/record/<record_id>/density/<density_id>",
           methods=['GET', 'DELETE', 'PUT'])
@auth.login_required
def density(record_id, density_id):
    if request.method == 'GET':
        record = app.session.query(Record).\
            join(RecordDensity).\
            filter(RecordDensity.id == density_id).one()
        data = json.dumps(RecordSchema().dump(record))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(RecordDensity).\
            filter(RecordDensity.id == density_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return 'deleted'
    elif request.method == 'PUT':
        data = json.loads(request.json)
        date = data["date"]
        label = data["label"]
        value = data["value"]
        query = app.session.query(RecordDensity).\
            filter_by(id=density_id).\
            one()
        query.date = date
        query.label = label
        query.value = value
        app.session.commit()
        return 'edited density'


@app.route("/records/conditionings",
           methods=['GET'])
@auth.login_required
def all_conditionings():
    conditionings = app.session.query(Conditioning).\
        join(ConditioningEntry).all()
    data = json.dumps(ConditioningEntriesSchema(many=True).dump(conditionings))
    return data


@app.route("/record/<record_id>/conditionings",
           methods=['GET', 'POST'])
@auth.login_required
def conditionings(record_id):
    if request.method == 'GET':
        conditionings = app.session.query(Conditioning).\
            filter(Conditioning.record == record_id).all()
        data = json.dumps(ConditioningSchema(many=True).dump(conditionings))
        return data
    elif request.method == 'POST':
        data = json.loads(request.json)
        date = data["date"]
        try:
            commit = data["commit"]
        except KeyError:
            commit = True
        new_conditioning = Conditioning(record=record_id,
                                        date=date)
        app.session.add(new_conditioning)
        app.session.flush()
        new_id = new_conditioning.id
        record = app.session.query(Record).\
            filter(Record.id == record_id).one()
        record.conditionned = True
        if commit:
            app.session.commit()
        return {"new_id": new_id}


@app.route("/conditioning/<conditioning_id>",
           methods=['GET', 'DELETE', 'PUT'])
@auth.login_required
def conditioning(conditioning_id):
    if request.method == 'GET':
        conditioning = app.session.query(Conditioning).\
            join(ConditioningEntry).\
            filter(Conditioning.id == conditioning_id).one()
        data = json.dumps(ConditioningSchema().dump(conditioning))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(Conditioning).\
            filter(Conditioning.id == conditioning_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return 'deleted'
    elif request.method == 'PUT':
        data = json.loads(request.json)
        date = data["date"]
        query = app.session.query(Conditioning).\
            filter_by(id=conditioning_id).\
            one()
        query.date = date
        app.session.commit()
        return 'edited conditioning'


@app.route("/record/<record_id>/conditioning_entries",
           methods=['GET'])
@auth.login_required
def all_conditioning_entries(record_id):
    conditionings = app.session.query(Conditioning).\
        join(ConditioningEntry).\
        filter(Conditioning.record == record_id).all()
    data = json.dumps(ConditioningEntriesSchema(many=True).dump(conditionings))
    return data


@app.route("/record/<record_id>/conditioning_entries/<conditioning_id>",
           methods=['GET', 'POST'])
@auth.login_required
def conditioning_entries(record_id, conditioning_id):
    if request.method == 'GET':
        record = app.session.query(Conditioning).\
            join(ConditioningEntry).\
            filter(Conditioning.record == record_id and
                   Conditioning.id == conditioning_id).all()
        data = json.dumps(ConditioningEntriesSchema(many=True).dump(record))
        return data
    elif request.method == 'POST':
        data = json.loads(request.json)
        volume = data["volume"]
        quantity = data["quantity"]
        try:
            commit = data["commit"]
        except KeyError:
            commit = True
        new_entry = ConditioningEntry(conditioning=conditioning_id,
                                      volume=volume,
                                      quantity=quantity)
        app.session.add(new_entry)
        app.session.flush()
        new_id = new_entry.id
        if commit:
            app.session.commit()
        return {"new_id": new_id}


@app.route("/conditioning_entry/<entry_id>",
           methods=['GET', 'DELETE', 'PUT'])
@auth.login_required
def conditioning_entry(entry_id):
    if request.method == 'GET':
        entry = app.session.query(ConditioningEntry).\
            filter(ConditioningEntry.id == entry_id).one()
        data = json.dumps(ConditioningEntrySchema().dump(entry))
        return data
    elif request.method == 'DELETE':
        query = app.session.query(ConditioningEntry).\
            filter(ConditioningEntry.id == entry_id).\
            one()
        app.session.delete(query)
        app.session.commit()
        return 'deleted'
    elif request.method == 'PUT':
        data = json.loads(request.json)
        volume = data["volume"]
        quantity = data["quantity"]
        query = app.session.query(ConditioningEntry).\
            filter_by(id=entry_id).\
            one()
        query.volume = volume
        query.quantity = quantity
        app.session.commit()
        return 'edited conditioning entry'
