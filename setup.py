from setuptools import setup

setup(
   name='bibrew_server',
   version='0.9.0',
   author='Dr Loiseau',
   author_email='dr.loiseau@lebib.org',
   packages=['bibrew_server'],
   scripts=[],
   url='',
   license='LICENSE.txt',
   description='Server side logic for bibrew : brew helper',
   long_description='',
   install_requires=[
       "flask",
       "flask_httpauth",
       "werkzeug",
       "sqlalchemy",
       "marshmallow_sqlalchemy",
       "marshmallow_enum",
       "psycopg2-binary",
       "numpy",
       "logbook"
   ],
)
